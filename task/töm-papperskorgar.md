# Töm papperskorg

Längst fram i elektroniklabbet (A007) finns det fyra papperskorgar.

  * *Bränbart* => Lägg i kontainer för brännbart.
  * *Deponi* => Lägg i kontainer för Deponi
  * *Elektronik* => Ställ det hos vaktmästaren. 
  * *Papper* => Släng i pappersåtervining.
