# Installer eclipse

1. Gå till http://www.eclipse.org/downloads/index.php och ladda ned rätt fil. Den borde hete något liknade *filen.tar.gz*.
2. Skapa en map för eclipse: *mkdir ~/eclipse*
3. Packa upp filen du laddad ned i den mappen:
*tar -zxvf filen.tar.gz -C ~/eclipse*
4. Testa att köra programmet genom att köra den körbara filen i mapen du nyss skapade:
*~/eclipse/eclipse-version/eclipse/eclipse*
5.  Skapa en symbolisk länk till eclipse:
*sudo ln -s /home/htsit/eclipse/eclipse-version/eclipse/eclipse  /usr/bin/*
