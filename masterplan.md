# Plan för ordning i labsalarna på it inriktningen

  * Städa rullborden.
  * Ställ alla osorterade lådor på rullborden.
  * Flytta krm arbetsplats till A007.
  * Installera OS på dator på krm:s arbetsplats.
  * Städa bordet i A006.
  * Flytta in alla skärmar i ett skåp.
  * Städa upp allt på borden i A007.
  * Flytta in alla lådor på borden i A007 till bord A006 eller skåp.
  * Städa bort allt från borden i A007.
  * Gå igenom all skåp och ställ in lådor.
  * Stäng alla skåp och notera vad som finns i dem.
  * Se till att få ett nytt bord till A006.
  * Be om att få ett nya hyllor vid vägen till korridoren i A007.
  * Gör oss av med det lilla datorbordet.
  * Ta bort alla datorer ifrån A006 till nya hyllan alternativ skåp.
  * Ta bort skrivborden ifrån A006.
  * Flytta in routern i A006.
  * Flytta in server racketen i A006.
  * Flytta min arbetsplats till A007.
  * Fixa problemmet med nätverket i A008.
  * Gör en "task" för vanliga arbetsuppgifter. 
  * Göt "task" och "issues" som det finns behov av att utföra nu i labsalen.

När det passa gör:

  * Lägg uppgifter som behöver göras på htsit-lab
  * Skapa git arbetsuppgift projekt.
  * Skapa regler HTSIT som gynnar att hjäpa till.
  * Sammankoppla dessa med kunskapskriterier för Datorteknik.
  
